/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

export default function imageReducer(
    state = {
        images: [],
        page: 0,
        pages: 0,
        toDelete: '',
        loading: true,
        lightbox: null
    },
    action
) {
    switch (action.type) {
        case 'LOAD_IMAGES': {
            const { list, pages } = action.payload
            return { ...state, images: list, pages }
        }
        case 'CONFIRM_DELETE_IMAGE': {
            return { ...state, toDelete: action.payload }
        }
        case 'SET_LOADING': {
            return { ...state, loading: action.payload }
        }
        case 'SET_PAGE': {
            return { ...state, page: action.payload }
        }
        case 'SET_LIGHTBOX': {
            return { ...state, lightbox: action.payload }
        }
        default:
            return state
    }
}
