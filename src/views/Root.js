/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react'
import PropTypes from 'prop-types'
import { hot } from 'react-hot-loader'
import { Route, Router, Switch } from 'react-router-dom'
import connect from 'react-redux/es/connect/connect'
import LoginComponent from '../components/LoginComponent'
import { readToken } from '../actions/login_actions'
import history from '../history'
import RegisterComponent from '../components/RegisterComponent'
import App from './App'

const mapStateToProps = state => ({ auth: state.login })

const mapDispatchesToProps = {
    init: readToken
}

@connect(
    mapStateToProps,
    mapDispatchesToProps
)
class Root extends React.Component {
    static propTypes = {
        init: PropTypes.func.isRequired
    }

    componentDidMount() {
        const { init } = this.props
        init()
    }

    render() {
        return (
            <div>
                <Router history={history}>
                    <Switch>
                        <Route path="/login" component={LoginComponent} />
                        <Route path="/register" component={RegisterComponent} />
                        <Route path="/:page?" component={App} />
                    </Switch>
                </Router>
            </div>
        )
    }
}

export default hot(module)(Root)
