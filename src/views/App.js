/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react'
import PropTypes from 'prop-types'
import connect from 'react-redux/es/connect/connect'
import ImageGrid from '../components/ImageGrid'
import { withRouter } from 'react-router'
import { setImagesPage } from '../actions/image_actions'
import FixedHeader from '../components/FixedHeader'
import InfoPage from '../components/InfoPage'

const mapStateToProps = ({ images, app }) => ({
    view: app.view
})

const mapDispatchesToProps = {
    setPage: setImagesPage
}

@connect(
    mapStateToProps,
    mapDispatchesToProps
)
class App extends React.Component {
    static propTypes = {
        view: PropTypes.string.isRequired,
        setPage: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)
        const { setPage } = this.props
        const pathParam = this.props.match.params.page
        const pathOrDefault =
            isNaN(pathParam) || +pathParam <= 0 ? 1 : +pathParam
        setPage(pathOrDefault - 1)
    }

    render() {
        const { view } = this.props
        return (
            <div>
                <FixedHeader />
                <div className={'viewcontainer'}>
                    {view === 'image' && <ImageGrid />}
                    {view === 'info' && <InfoPage />}
                </div>
            </div>
        )
    }
}

export default withRouter(App)
