/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import axios from 'axios'
import history from '../history'

export function loadImages(images) {
    return {
        type: 'LOAD_IMAGES',
        payload: images
    }
}

export function confirmDeleteImage(url) {
    return {
        type: 'CONFIRM_DELETE_IMAGE',
        payload: url
    }
}

export function setLoading(bool) {
    return {
        type: 'SET_LOADING',
        payload: bool
    }
}

export function setImagesPage(page) {
    return {
        type: 'SET_PAGE',
        payload: page
    }
}

export function setLightboxImage(src) {
    return {
        type: 'SET_LIGHTBOX',
        payload: src
    }
}

export function deleteRequest(token, page, imageUrl) {
    const url = '/api/images/delete'
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        },
        params: {
            image: imageUrl
        }
    }
    return dispatch => {
        axios.post(url, { image: imageUrl }, config).then(result => {
            dispatch(getImagesRequest(token, page))
            dispatch(confirmDeleteImage(''))
        })
    }
}

export function getImagesRequest(token, page) {
    const url = '/api/images/my-images'
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        },
        params: {
            page: page
        }
    }
    return dispatch => {
        dispatch(setLoading(true))
        axios
            .get(url, config)
            .then(result => {
                const data = {
                    list: result.data.images.sort(
                        (a, b) => b.createdAt - a.createdAt
                    ),
                    pages: result.data.pages
                }
                dispatch(loadImages(data))
                if (page > data.pages) {
                    dispatch(setImagesPage(data.pages))
                    history.push('/' + (data.pages + 1))
                } else {
                    dispatch(setImagesPage(page))
                    history.push('/' + (page + 1))
                }
                dispatch(setLoading(false))
            })
            .catch(error => console.log(error))
    }
}
