/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import axios from 'axios'
import history from '../history'
import {getImagesRequest} from './image_actions'

export function loginFailure(error) {
    return {
        type: 'LOGIN_FAILURE',
        payload: error
    }
}

export function loginSuccess(user) {
    return {
        type: 'LOGIN_SUCCESS',
        payload: user
    }
}

export function loginRequest(username, password) {
    const url = '/api/users/login'
    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    return dispatch => {
        const formData = new FormData()
        formData.set('username', username)
        formData.set('password', password)
        axios
            .post(url, formData, config)
            .then(response => {
                dispatch(writeToken(response.data.token))
                dispatch(loginSuccess(response.data.user))
                dispatch(getImagesRequest(response.data.token, 0))
            })
            .catch(error => dispatch(loginFailure(error.message)))
    }
}

export function tokenLoginRequest(token) {
    const url = '/api/users/get-user-with-token'
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    }
    return dispatch => {
        axios
            .get(url, config)
            .then(response => dispatch(loginSuccess(response.data)))
            .catch(error => {
                dispatch(loginFailure(error.message))
                if (error.response.status === 500) {
                    history.push('/login')
                    dispatch(writeToken(null))
                }
            })
    }
}

export function readTokenSuccess(token) {
    return {
        type: 'READ_TOKEN_SUCCESS',
        payload: token
    }
}

export function readToken() {
    return dispatch => {
        const token = localStorage.getItem('token')
        if (token !== null) {
            dispatch(readTokenSuccess(token))
            dispatch(tokenLoginRequest(token))
            // if (history.location.pathname === '/') {
            //     history.push('/1')
            // }
        } else if (history.location.pathname !== '/login') {
            history.push('/login')
        }
    }
}

export function writeToken(token) {
    return dispatch => {
        if (token !== null) {
            localStorage.setItem('token', token)
            dispatch(readTokenSuccess(token))
        } else {
            localStorage.removeItem('token')
            history.push('/login')
        }
    }
}
