/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import axios from 'axios'
import { loginRequest } from './login_actions'

export function registerFailure(error) {
    return {
        type: 'REGISTER_FAILURE',
        payload: error
    }
}

export function registerSuccess(user) {
    return {
        type: 'REGISTER_SUCCESS',
        payload: user
    }
}

export function registerRequest(username, email, password) {
    const url = '/api/users/sign-up'
    return dispatch => {
        axios
            .post(url, { username, email, password })
            .then(response => {
                dispatch(registerSuccess(response.data))
                dispatch(loginRequest(username, password))
            })
            .catch(error => {
                console.log('ERROR: ', error)
                dispatch(registerFailure(error.message))
            })
    }
}
