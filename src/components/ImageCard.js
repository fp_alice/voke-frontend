/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Icon } from 'semantic-ui-react'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import LightBox from './LightBox'
import './ImageCard.css'

export default class ImageCard extends React.Component {
    static propTypes = {
        image: PropTypes.object.isRequired,
        token: PropTypes.string.isRequired,
        page: PropTypes.number.isRequired,
        toDelete: PropTypes.string.isRequired,
        setToDelete: PropTypes.func.isRequired,
        deleteImage: PropTypes.func.isRequired,
        setLightbox: PropTypes.func.isRequired,
        lightbox: PropTypes.string
    }

    render() {
        const protocol = window.location.protocol
        const base = window.location.hostname
        const {
            image,
            token,
            page,
            toDelete,
            setToDelete,
            deleteImage,
            setLightbox,
            lightbox
        } = this.props
        const location = `/i/${image.url}.${image.extension}`
        return (
            <div className={'gridcard'}>
                <Card key={image.url}>
                    <LightBox
                        src={location}
                        current={lightbox}
                        setLightbox={setLightbox}
                    />
                    <div
                        className={'imagewrapper'}
                        onClick={() => setLightbox(location)}>
                        <img src={location} className={'blur'} />
                        <img src={location} className={'cardimage'} />
                    </div>
                    <Card.Content>
                        <div className={'imagebuttons'}>
                            {image.url === toDelete ? (
                                <Button.Group widths={'2'}>
                                    <Button
                                        className={'textwrapper'}
                                        onClick={() => setToDelete('')}>
                                        <Icon name={'dont'} />
                                        <span className={'buttontext'}>
                                            Cancel
                                        </span>
                                    </Button>
                                    <Button.Or />
                                    <Button
                                        className={'textwrapper'}
                                        onClick={() => {
                                            deleteImage(token, page, toDelete)
                                        }}
                                        negative>
                                        <Icon name={'trash'} />
                                        <span className={'buttontext'}>
                                            Delete
                                        </span>
                                    </Button>
                                </Button.Group>
                            ) : (
                                <Button.Group widths={'2'}>
                                    <CopyToClipboard
                                        text={
                                            protocol + '//' + base + location
                                        }>
                                        <Button className={'textwrapper'}>
                                            <Icon name={'linkify'} />
                                            <span className={'buttontext'}>
                                                Link
                                            </span>
                                        </Button>
                                    </CopyToClipboard>
                                    <Button
                                        className={'textwrapper'}
                                        onClick={() => setToDelete(image.url)}>
                                        <Icon name={'delete'} />
                                        <span className={'buttontext'}>
                                            Delete
                                        </span>
                                    </Button>
                                </Button.Group>
                            )}
                        </div>
                    </Card.Content>
                </Card>
            </div>
        )
    }
}
