/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Icon, Input, Menu } from 'semantic-ui-react'
import { headerSetView } from '../actions/app_actions'
import { writeToken } from '../actions/login_actions'
import { getImagesRequest, setImagesPage } from '../actions/image_actions'
import './Header.css'

const mapStateToProps = ({ images, app, login }) => ({
    page: images.page,
    pages: images.pages,
    view: app.view,
    user: login.user,
    token: login.token
})

const mapDispatchesToProps = {
    setView: headerSetView,
    setToken: writeToken,
    setPage: getImagesRequest,
    setPageNum: setImagesPage
}

@connect(
    mapStateToProps,
    mapDispatchesToProps
)
export default class FixedHeader extends React.Component {
    static propTypes = {
        page: PropTypes.number.isRequired,
        pages: PropTypes.number.isRequired,
        view: PropTypes.string.isRequired,
        user: PropTypes.object.isRequired,
        setToken: PropTypes.func.isRequired,
        setView: PropTypes.func.isRequired,
        setPage: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)
        this.state = {
            input: '',
            error: false
        }
    }

    menuClick = title => {
        const { setView, view } = this.props
        return () => {
            if (view !== title) {
                setView(title)
            }
        }
    }

    handleChange = e => {
        const { pages } = this.props
        const value = e.target.value
        if (!isNaN(value) && +value <= pages + 1 && +value > 0) {
            this.setState({ input: value, error: false })
        } else {
            this.setState({ error: true })
        }
    }

    handleSubmit = e => {
        const { token, setPage } = this.props
        const { input, error } = this.state
        if (e.charCode === 13 && !error) {
            setPage(token, +input - 1)
        }
    }

    renderViewMenu = () => {
        const { view, setToken } = this.props
        return (
            <Menu.Menu position={'left'}>
                <Menu.Item
                    name={'image'}
                    active={view === 'image'}
                    onClick={this.menuClick('image')}>
                    <Icon name={'image'} />
                    Images
                </Menu.Item>
                <Menu.Item
                    name={'info'}
                    active={view === 'info'}
                    onClick={this.menuClick('info')}>
                    <Icon name={'question'} />
                    Info
                </Menu.Item>
                <Menu.Item onClick={() => setToken(null)}>
                    <Icon name={'log out'} />
                    Logout
                </Menu.Item>
            </Menu.Menu>
        )
    }

    renderPageNumber = () => {
        const { page, pages, view } = this.props
        const showPages = view === 'image'
        return (
            showPages && (
                <Menu.Item>
                    <Icon name={'home'} />
                    {page + 1} / {pages + 1}
                </Menu.Item>
            )
        )
    }

    renderUser = () => {
        const { user } = this.props
        return (
            <Menu.Item>
                <Icon name={'user'} />
                {user.username}
            </Menu.Item>
        )
    }

    renderLeftNavigation = () => {
        const { page, token, setPage } = this.props
        return (
            <Button.Group>
                <Button
                    inverted
                    attached={'right'}
                    onClick={() => {
                        setPage(token, 0)
                    }}
                    active={false}
                    disabled={page === 0}>
                    <Icon name={'angle double left'} />
                </Button>
                <Button
                    inverted
                    attached={'right'}
                    onClick={() => {
                        setPage(token, page - 1)
                    }}
                    active={false}
                    disabled={page === 0}>
                    <Icon name={'angle left'} />
                </Button>
            </Button.Group>
        )
    }

    renderRightNavigation = () => {
        const { page, pages, token, setPage } = this.props
        return (
            <Button.Group>
                <Button
                    inverted
                    attached={'left'}
                    onClick={() => {
                        setPage(token, page + 1)
                    }}
                    active={false}
                    disabled={page >= pages}>
                    <Icon name={'angle right'} />
                </Button>
                <Button
                    inverted
                    attached={'left'}
                    onClick={() => {
                        setPage(token, pages)
                    }}
                    active={false}
                    disabled={page >= pages}>
                    <Icon name={'angle double right'} />
                </Button>
            </Button.Group>
        )
    }

    renderPageNavigation = () => {
        const { view, pages } = this.props
        const showPages = view === 'image'
        const showNavigation = pages !== 0 && showPages
        return (
            <Menu.Menu position={'right'}>
                {showNavigation && (
                    <Menu.Item className={'imagepagenav'}>
                        <span>
                            <Input
                                size={'tiny'}
                                inverted
                                type={'text'}
                                name={'input'}
                                onChange={this.handleChange}
                                error={this.state.error}
                                onKeyPress={this.handleSubmit}>
                                {this.renderLeftNavigation()}
                                <input />
                                {this.renderRightNavigation()}
                            </Input>
                        </span>
                    </Menu.Item>
                )}
                {this.renderPageNumber()}
                {this.renderUser()}
            </Menu.Menu>
        )
    }

    render() {
        return (
            <Menu fixed={'top'} inverted>
                {this.renderViewMenu()}
                {this.renderPageNavigation()}
            </Menu>
        )
    }
}
