/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Button, Form, Container } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { loginRequest } from '../actions/login_actions'
import history from '../history'
import './Login.css'

const mapStateToProps = state => ({ ...state.login })

const mapDispatchesToProps = { login: loginRequest }

@connect(
    mapStateToProps,
    mapDispatchesToProps
)
export default class LoginComponent extends React.Component {
    static propTypes = {
        login: PropTypes.func.isRequired
    }

    constructor() {
        super()

        this.state = {
            username: '',
            password: ''
        }
    }

    handleChange = (e, { name, value }) => {
        this.setState({ [name]: value })
    }

    handleSubmit = () => {
        const { username, password } = this.state
        const { login } = this.props
        login(username, password)
    }

    render() {
        const { username, password } = this.state
        return (
            <Container className={'auth'}>
                <div className={'login'}>
                    <Form>
                        <Form.Input
                            label="Username"
                            placeholder="Username"
                            name="username"
                            value={username}
                            onChange={this.handleChange}
                        />
                        <Form.Input
                            label="Password"
                            placeholder="Password"
                            name="password"
                            value={password}
                            type="password"
                            onChange={this.handleChange}
                        />
                        <Button.Group widths={'2'}>
                            <Button onClick={this.handleSubmit}>Login</Button>
                            <Button.Or />
                            <Button onClick={() => history.push('/register')}>
                                Need to sign up?
                            </Button>
                        </Button.Group>
                    </Form>
                </div>
            </Container>
        )
    }
}
