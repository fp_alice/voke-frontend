/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Form, Button, Container } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { registerRequest } from '../actions/register_actions'
import history from '../history'
import './Login.css'

const mapStateToProps = state => ({ ...state.register })

const mapDispatchesToProps = { register: registerRequest }

@connect(
    mapStateToProps,
    mapDispatchesToProps
)
export default class RegisterComponent extends React.Component {
    static propTypes = {
        register: PropTypes.func.isRequired
    }

    constructor() {
        super()

        this.state = {
            username: '',
            email: '',
            password: '',
            passwordCheck: ''
        }
    }

    handleChange = (e, { name, value }) => {
        this.setState({ [name]: value })
    }

    handleSubmit = () => {
        const { username, password, passwordCheck, email } = this.state
        const { register } = this.props
        if (passwordCheck === password && password !== '') {
            register(username, email, password)
        }
    }

    render() {
        const { username, password, passwordCheck, email } = this.state
        return (
            <Container className={'auth'}>
                <div className={'login'}>
                    <Form>
                        <Form.Input
                            label="Email"
                            placeholder="Email"
                            name="email"
                            value={email}
                            onChange={this.handleChange}
                        />
                        <Form.Input
                            label="Username"
                            placeholder="Username"
                            name="username"
                            value={username}
                            onChange={this.handleChange}
                        />
                        <Form.Input
                            label="Password"
                            placeholder="Password"
                            name="password"
                            value={password}
                            type="password"
                            onChange={this.handleChange}
                        />
                        <Form.Input
                            label="Repeat password"
                            placeholder="Password"
                            name="passwordCheck"
                            value={passwordCheck}
                            type="password"
                            onChange={this.handleChange}
                        />
                        <Button.Group widths={'2'}>
                            <Button onClick={this.handleSubmit}>
                                Register
                            </Button>
                            <Button.Or />
                            <Button onClick={() => history.push('/login')}>
                                Need to sign in?
                            </Button>
                        </Button.Group>
                    </Form>
                </div>
            </Container>
        )
    }
}
