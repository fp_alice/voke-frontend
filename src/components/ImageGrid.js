/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react'
import PropTypes from 'prop-types'
import ImageCard from './ImageCard'
import {
    confirmDeleteImage,
    deleteRequest,
    getImagesRequest,
    setLightboxImage
} from '../actions/image_actions'
import { connect } from 'react-redux'
import './ImageGrid.css'

const mapStateToProps = state => ({
    token: state.login.token,
    ...state.images
})

const mapDispatchesToProps = {
    getImages: getImagesRequest,
    setToDelete: confirmDeleteImage,
    deleteImage: deleteRequest,
    setLightbox: setLightboxImage
}

@connect(
    mapStateToProps,
    mapDispatchesToProps
)
export default class ImageGrid extends React.Component {
    static propTypes = {
        images: PropTypes.array.isRequired,
        token: PropTypes.string.isRequired,
        page: PropTypes.number.isRequired,
        toDelete: PropTypes.string.isRequired,
        setToDelete: PropTypes.func.isRequired,
        deleteImage: PropTypes.func.isRequired,
        loading: PropTypes.bool.isRequired,
        setLightbox: PropTypes.func.isRequired
    }

    componentDidUpdate(prevProps) {
        const { token, getImages, page } = this.props
        if (!prevProps.token && prevProps.token !== token) {
            getImages(token, page)
        }
    }

    render() {
        const {
            images,
            token,
            page,
            toDelete,
            setToDelete,
            deleteImage,
            loading,
            setLightbox,
            lightbox
        } = this.props

        return (
            !loading && (
                <div className={'grid'}>
                    {images.map(image => (
                        <ImageCard
                            key={image.url}
                            image={image}
                            token={token}
                            page={page}
                            toDelete={toDelete}
                            setToDelete={setToDelete}
                            deleteImage={deleteImage}
                            setLightbox={setLightbox}
                            lightbox={lightbox}
                        />
                    ))}
                </div>
            )
        )
    }
}
