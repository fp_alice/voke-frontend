/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react'
import { connect } from 'react-redux'
import { Container, Header } from 'semantic-ui-react'

const mapStateToProps = ({ login }) => ({
    user: login.user
})

const mapDispatchesToProps = {}

@connect(
    mapStateToProps,
    mapDispatchesToProps
)
export default class InfoPage extends React.Component {
    render() {
        const { user } = this.props
        const { username, key } = user
        const location = window.location.host
        const script =
            '#!/bin/bash\n' +
            'curl -F "key=' +
            key +
            '" -F "file=@$1" ' +
            location +
            '/api/images/upload'
        return (
            <Container text>
                <Header>Hello, {username}</Header>
                <p>
                    Your key is <code>{key}</code>, <b>keep this secret</b>.
                    <br />
                    Here's an example upload script for you.
                </p>
                <pre>{script}</pre>
            </Container>
        )
    }
}
