/*
 * Copyright (C) 2018 Alice L.
 * This file is part of Voke <https://github.com/antflga/voke-frontend>.
 *
 * Voke is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voke is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voke. If not, see <http://www.gnu.org/licenses/>.
 */

const path = require('path')
const Hwp = require('html-webpack-plugin')

module.exports = {
    entry: [
        'webpack-dev-server/client?http://localhost:3000/',
        'webpack/hot/only-dev-server',
        './src/index.js'
    ],
    output: {
        filename: 'voke.js',
        path: path.join(__dirname, '/dist')
        // publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node-modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/env', '@babel/react'],
                    plugins: ['transform-class-properties']
                }
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000'
            }
        ]
    },
    plugins: [
        new Hwp({
            template: path.join(__dirname, 'index.html')
        })
    ],
    devServer: {
        host: 'localhost',
        port: 3000,
        https: false,
        historyApiFallback: true,
        hot: true,
        inline: true,
        proxy: {
            '/api/**': {
                target: 'http://localhost:8080/',
                changeOrigin: true,
                secure: false
            },
            '/i/**': {
                target: 'http://localhost:8080/',
                changeOrigin: true,
                secure: false
            }
        }
    }
}
